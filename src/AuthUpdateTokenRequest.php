<?php

namespace FritsStegmann\Laravel\JWT;

use Illuminate\Foundation\Http\FormRequest;

class AuthUpdateTokenRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'refresh_token' => 'required',
        ];
    }

    public function refreshToken()
    {
        return $this->get('refresh_token');
    }
}
