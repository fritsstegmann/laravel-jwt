<?php

namespace FritsStegmann\Laravel\JWT;

use Lcobucci\JWT\Parser;
use Lcobucci\JWT\ValidationData;
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Signer\Hmac\Sha384;

class JWTService
{
    private $accessTokenSecret;
    private $refreshTokenSecret;
    private $issuerId;
    private $ttl;

    /**
     * JWTService constructor.
     * @param $accessTokenSecret
     * @param $refreshTokenSecret
     * @param $issuerId
     * @param $ttl
     */
    public function __construct($accessTokenSecret, $refreshTokenSecret, $issuerId, $ttl)
    {
        $this->accessTokenSecret = hash('sha512', hash('sha512', $accessTokenSecret));
        $this->refreshTokenSecret = hash('sha512', hash('sha512', $refreshTokenSecret));
        $this->issuerId = $issuerId;
        $this->ttl = $ttl;
    }


    public function createRefreshToken(string $accessToken): string
    {
        $refresh = (new Builder())
            ->setId($this->encryptValue($this->getIssuerId()))
            ->setIssuedAt(time())
            ->set('uid', $accessToken)
            ->sign(new Sha384(), $this->getRefreshTokenSecret())
            ->getToken();

        return (string)$refresh;
    }

    public function createAccessToken(string $uid): string
    {
        $token = (new Builder())
            ->setId($this->encryptValue($this->getIssuerId()))
            ->setIssuedAt(time())
            ->setExpiration(time() + $this->getTTL())
            ->set('uid', $this->encryptValue($uid))
            ->sign(new Sha384(), $this->getAccessTokenSecret())
            ->getToken();

        return (string)$token;
    }

    public function validateRefreshToken($refreshToken): ?string
    {
        $refreshToken = (new Parser())->parse($refreshToken); // Parses from a string
        $originalAccessToken = (new Parser())->parse((string)$refreshToken->getClaim('uid')); // Parses from a string

        $data = new ValidationData(); // It will use the current time to validate (iat, nbf and exp)
        $data->setId($this->encryptValue($this->getIssuerId()));

        // we don't validate the $originalAccessToken because we already know it's invalid
        if ($refreshToken->validate($data) &&
            $refreshToken->verify(new Sha384(), $this->getRefreshTokenSecret()) &&
            $originalAccessToken->verify(new Sha384(), $this->getAccessTokenSecret())
        ) {
            $originalUid = $this->decryptValue($originalAccessToken->getClaim('uid'));
            return $originalUid;
        } else {
            return null;
        }
    }

    /**
     * @param string|null $getTokenForRequest
     * @return string|null
     */
    public function validateAccessTokenAndReturnUid(?string $getTokenForRequest): ?string
    {
        if ($getTokenForRequest == null) {
            return null;
        }

        $originalAccessToken = (new Parser())->parse($getTokenForRequest);

        $data = new ValidationData(); // It will use the current time to validate (iat, nbf and exp)
        $data->setId($this->encryptValue($this->getIssuerId()));

        if ($originalAccessToken->validate($data) &&
            $originalAccessToken->verify(new Sha384(), $this->getAccessTokenSecret())
        ) {
            return $this->decryptValue($originalAccessToken->getClaim('uid'));
        } else {
            return null;
        }
    }

    private function encryptValue(string $rawValue): string
    {
        $password = config('app.key');
        $password = hash('sha512', hash('sha512', $password));

        $method = 'aes-256-cbc';
        $iv = implode("", [
            chr(0x0),
            chr(0x0),
            chr(0x0),
            chr(0x0),
            chr(0x0),
            chr(0x0),
            chr(0x0),
            chr(0x0),
            chr(0x0),
            chr(0x0),
            chr(0x0),
            chr(0x0),
            chr(0x0),
            chr(0x0),
            chr(0x0),
            chr(0x0),
        ]);

        $encrypted = base64_encode(openssl_encrypt($rawValue, $method, $password, OPENSSL_RAW_DATA, $iv));

        return $encrypted;
    }

    private function decryptValue(string $encryptedValue): string
    {
        $password = config('app.key');
        $password = hash('sha512', hash('sha512', $password));

        $method = 'aes-256-cbc';
        $iv = implode("", [
            chr(0x0),
            chr(0x0),
            chr(0x0),
            chr(0x0),
            chr(0x0),
            chr(0x0),
            chr(0x0),
            chr(0x0),
            chr(0x0),
            chr(0x0),
            chr(0x0),
            chr(0x0),
            chr(0x0),
            chr(0x0),
            chr(0x0),
            chr(0x0),
        ]);

        $encrypted = openssl_decrypt(base64_decode($encryptedValue), $method, $password, OPENSSL_RAW_DATA, $iv);

        return $encrypted;
    }

    private function getAccessTokenSecret(): string
    {
        return $this->accessTokenSecret;
    }

    private function getRefreshTokenSecret(): string
    {
        return $this->refreshTokenSecret;
    }

    private function getTTL(): int
    {
        return $this->ttl;
    }

    private function getIssuerId(): string
    {
        return $this->issuerId;
    }
}
