<?php

namespace FritsStegmann\Laravel\JWT;

use Illuminate\Auth\GuardHelpers;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\UserProvider;
use Illuminate\Http\Request;

class JwtGuard implements Guard
{
    use GuardHelpers;

    protected $request;

    private $name;
    private $JWTService;

    /**
     * Create a new authentication guard.
     *
     * @param $name
     * @param JWTService $JWTService
     * @param UserProvider $provider
     * @param Request $request
     */
    public function __construct($name, JWTService $JWTService, UserProvider $provider = null, Request $request = null)
    {
        $this->name = $name;
        $this->request = $request;
        $this->provider = $provider;
        $this->JWTService = $JWTService;
    }

    /**
     * Get the currently authenticated user.
     *
     * @return Authenticatable|null
     */
    public function user()
    {
        // If we've already retrieved the user for the current request we can just
        // return it back immediately. We do not want to fetch the user data on
        // every call to this method because that would be tremendously slow.
        if (!is_null($this->user)) {
            return $this->user;
        }

        $user = null;

        $uid = $this->JWTService->validateAccessTokenAndReturnUid($this->getTokenForRequest());

        if ($uid != null) {
            $user = $this->provider->retrieveById($uid);
        }

        $this->user = $user;

        return $user;
    }

    /**
     * Get the token for the current request.
     *
     * @return string|null
     */
    public function getTokenForRequest(): ?string
    {
        return $this->request->bearerToken();
    }

    /**
     * Validate a user's credentials.
     *
     * @param array $credentials
     * @return bool
     */
    public function validate(array $credentials = [])
    {
        if ($this->provider->retrieveByCredentials($credentials)) {
            return true;
        }

        return false;
    }

    /**
     * Set the current request instance.
     *
     * @param Request $request
     * @return $this
     */
    public function setRequest(Request $request)
    {
        $this->request = $request;

        return $this;
    }
}
