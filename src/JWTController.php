<?php

namespace FritsStegmann\Laravel\JWT;

use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;

class JWTController extends Controller
{
    /**
     * @var JWTService
     */
    private $service;

    /**
     * JWTController constructor.
     * @param JWTService $service
     */
    public function __construct(JWTService $service)
    {
        $this->service = $service;
    }

    public function login(AuthLoginRequest $request)
    {
        $email = $request->email();
        $password = $request->password();

        $canLogin = Auth::attempt([
            'email' => $email,
            'password' => $password,
        ]);

        if ($canLogin) {
            $token = $this->service->createAccessToken(Auth::user()->getAuthIdentifier());
            $refresh = $this->service->createRefreshToken($token);

            return [
                'access_token' => $token,
                'refresh' => $refresh,
            ];
        } else {
            return Response(["message" => "Unauthenticated."], 401);
        }
    }

    public function updateToken(AuthUpdateTokenRequest $request)
    {
        $uid = $this->service->validateRefreshToken($request->refreshToken());
        if ($uid) {
            return [
                'access_token' => $this->service->createAccessToken($uid)
            ];
        } else {
            return Response(["message" => "Unauthenticated."], 401);
        }
    }
}
