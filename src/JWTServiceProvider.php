<?php

namespace FritsStegmann\Laravel\JWT;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\ServiceProvider;

class JWTServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(JWTService::class, function ($app) {
            if ($app->config['app']['debug'] == false || $app->config['app']['env'] == 'production') {
                if ($app->config['jwt']['access_token_secret'] == 'secret' || $app->config['jwt']['refresh_token_secret'] == 'secret') {
                    exit('the server is set to production mode but the JWT settings is insecure');
                }
                if ($app->config['jwt']['access_token_secret'] == $app->config['jwt']['refresh_token_secret']) {
                    exit('please use different values for the JWT access and refresh secrets');
                }
            }
            return new JWTService(
                $app->config['jwt']['access_token_secret'],
                $app->config['jwt']['refresh_token_secret'],
                $app->config['jwt']['issuer_id'],
                $app->config['jwt']['ttl']
            );
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__ . '/../config/jwt.php' => config_path('jwt.php'),
        ]);

        $this->mergeConfigFrom(
            __DIR__ . '/../config/auth.php', 'auth'
        );

        $this->loadRoutesFrom(__DIR__ . '/../routes/routes.php');

        Auth::extend('jwt', function ($app, $name, array $config) {
            // Return an instance of Illuminate\Contracts\Auth\Guard...

            return new JwtGuard(
                $name,
                $app[JWTService::class],
                Auth::createUserProvider($config['provider']),
                $app['request']
            );
        });
    }
}
