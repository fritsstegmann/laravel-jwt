<?php

Route::prefix('api')
    ->middleware('api')
    ->group(function () {
        Route::post('/auth/jwt/login', '\FritsStegmann\Laravel\JWT\JWTController@login');
        Route::post('/auth/jwt/update_token', '\FritsStegmann\Laravel\JWT\JWTController@updateToken');
    });