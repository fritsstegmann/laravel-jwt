<?php

return [
    'access_token_secret' => env('JWT_ACCESS_TOKEN_SECRET', 'secret'),
    'refresh_token_secret' => env('JWT_REFRESH_TOKEN_SECRET', 'secret'),
    'issuer_id' => env('JWT_ISSUER_ID', 'secret'),
    'ttl' => env('JWT_TTL', 900),
];
